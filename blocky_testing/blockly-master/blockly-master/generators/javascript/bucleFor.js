Blockly.JavaScript['bucleFor'] = function(block) {
  // Repeat n times (internal number).
  var repeats = Number(block.getFieldValue('repeticiones'));
  var branch = Blockly.JavaScript.statementToCode(block, 'hacer');
  branch = Blockly.JavaScript.addLoopTrap(branch, block.id);
  var loopVar = Blockly.JavaScript.variableDB_.getDistinctName(
      'contador', Blockly.Variables.NAME_TYPE);
  var code = 'for (var ' + loopVar + ' = 0; ' +
      loopVar + ' < ' + repeats + '; ' +
      loopVar + '++) {\n' +
      branch + '}\n';
  return code;
};