function Mitai(color){
	this.color = color;
    this.setContenedor();
	this.contenedor.add(this.obtenerEsqueleto());
	this.contenedor.scale.set(0.05,0.05,0.05);
    this.metodos =['avanzar', 'girarDerecha', 'girarIzquierda', 'hayPare'];
}

Mitai.prototype = new Personaje();
Mitai.prototype.constructor = Mitai;

Mitai.prototype.obtenerEsqueleto = function(){
	var head = new THREE.SphereGeometry(32, 8, 8),
            hand = new THREE.SphereGeometry(8, 4, 4),
            foot = new THREE.SphereGeometry(16, 4, 4, 0, Math.PI * 2, 0, Math.PI / 2),
            nose = new THREE.SphereGeometry(4, 4, 4),
            // Set the material, the "skin",
			skinColor = new THREE.Color(this.color),
            material = new THREE.MeshNormalMaterial({color: skinColor}),
			mesh = new THREE.Object3D();
        // Set the character modelisation object
        mesh.position.y = 48;
        // Set and add its head
        head = new THREE.Mesh(head, material);
        head.position.y = 0;
        mesh.add(head);
        // Set and add its hands
        var hands = {
            left: new THREE.Mesh(hand, material),
            right: new THREE.Mesh(hand, material)
        };
        hands.left.position.x = -40;
        hands.left.position.y = -8;
        hands.right.position.x = 40;
        hands.right.position.y = -8;
        mesh.add(hands.left);
        mesh.add(hands.right);
        // Set and add its feet
        var feet = {
            left: new THREE.Mesh(foot, material),
            right: new THREE.Mesh(foot, material)
        };
        feet.left.position.x = -20;
        feet.left.position.y = -48;
        feet.left.rotation.y = Math.PI / 4;
        feet.right.position.x = 20;
        feet.right.position.y = -48;
        feet.right.rotation.y = Math.PI / 4;
        mesh.add(feet.left);
        mesh.add(feet.right);
        // Set and add its nose
        nose = new THREE.Mesh(nose, material);
        nose.position.y = 0;
        nose.position.z = 32;
        mesh.add(nose);

        return mesh;
}

//
Mitai.prototype.avanzarAnimacion = function(){
    /*this.setDireccion(steps);
    this.contenedor.position.x += this.getDireccion().x;
    this.contenedor.position.z += this.getDireccion().z;*/
    //obtener el tween para la animación
    var tween = new TWEEN.Tween({z:0}).to({z:141.6}, 500);
    var thisObjeto = this;
    var lastStep = 0;
    tween.onUpdate(function () {
        //escenaBasica.personajePrincipal.avanzar(this.z);

        thisObjeto.avanzar(this.z - lastStep);
        lastStep = this.z;

    });

    return tween;
}

//
Mitai.prototype.avanzar = function(steps){
    this.setDireccion(steps);
    this.contenedor.position.x += this.getDireccion().x;
    this.contenedor.position.z += this.getDireccion().z;
}

//
Mitai.prototype.girarDerechaAnimacion = function(){

    var tweenData = this.getRotationData(Personaje.DERECHA);
    var tween = new TWEEN.Tween(tweenData.start).to(tweenData.end, 500);
    var thisObjeto = this;
    tween.onUpdate(function () {
        //escenaBasica.personajePrincipal.avanzar(this.z);
        thisObjeto.girar(this.y);
    }).onComplete(function(){
        thisObjeto.setOrientacion(Personaje.DERECHA);
    });
    thisObjeto.setOrientacionData(Personaje.DERECHA);
    return tween;
}

//
Mitai.prototype.girarIzquierdaAnimacion = function(){
    var tweenData = this.getRotationData(Personaje.IZQUIERDA);
    var tween = new TWEEN.Tween(tweenData.start).to(tweenData.end, 500);
    var thisObjeto = this;
    tween.onUpdate(function () {
        //escenaBasica.personajePrincipal.avanzar(this.z);
        thisObjeto.girar(this.y);
    }).onComplete(function(){
        thisObjeto.setOrientacion(Personaje.IZQUIERDA);
    });
    thisObjeto.setOrientacionData(Personaje.IZQUIERDA);
    return tween;
}

//
Mitai.prototype.girar = function(angle){
    var angleForRotation = angle;
    var angleRadians = angleForRotation * (Math.PI / 180);
    this.contenedor.rotation.set(0, angleRadians, 0);
}

//
Mitai.prototype.tieneMetodo = function(metodo){
    var result = false;
    for(var key in this.metodos){
        if(this.metodos[key]==metodo){
            result = true;
            break;
        }
    }
    return result;
}

