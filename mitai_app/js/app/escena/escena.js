
var PROPIEDADES_CAMARA = {POSICION_PERSPECTIVA :{X: 0, Y: 10, Z: 100}, POSICION_ARRIBA: {X:0, Y:700, Z:0}, CONFIGURACION: {fov:45, aspect:1, near:0.1, far: 10000} },
LUCES= {color: new THREE.Color("#fff"), intensidad: 1.0};
var DEFAULT_CAMERA = '';
function Escena(){
	this.escenario = new THREE.Scene();//new Physijs.Scene();
	this.camara =  new THREE.PerspectiveCamera(PROPIEDADES_CAMARA.CONFIGURACION.fov, 
			PROPIEDADES_CAMARA.CONFIGURACION.aspect,
			PROPIEDADES_CAMARA.CONFIGURACION.near,
			PROPIEDADES_CAMARA.CONFIGURACION.far);

	this.configuracionCamara = {direccion:{arriba:false, perspectiva: true}};
	this.luces = {hemisferio : new THREE.HemisphereLight( new THREE.Color('#fff'), new THREE.Color('#000'), 1 ), direccional :new THREE.DirectionalLight(LUCES.color, LUCES.intensidad),  ambiente :  new THREE.AmbientLight(new THREE.Color('#656565')), fuentes: []};
	this.renderizador = new THREE.WebGLRenderer({antialias: true});
	this.loader = new THREE.ObjectLoader();
}


//iniciar la escena
Escena.prototype.inicializarEscena = function(archivoEscena, alturaEscena, anchoEscena){
	//configuramos el aspecto de la camara y la ubicación de la misma, la agregamos a la escena
	this.configurarAspecto(alturaEscena, anchoEscena);
	this.configurarCamara();
	this.escenario.add(this.camara);
	//cargamos nuestra escena desde un archivo externo
	this.cargarEscena(4,4);
	//agregamos las luces a nuestra escena
	this.luces.direccional.position.set(100,200,150);
	this.escenario.add(this.luces.direccional);

	this.escenario.add(this.luces.ambiente);
	//this.luces.hemisferio.position.set(0,150,0);
	//this.escenario.add(this.luces.hemisferio);
}

//configurar la camara
Escena.prototype.configurarCamara = function(){
	if(DEFAULT_CAMERA=='arriba'){
		this.configuracionCamara.direccion.arriba=true;
		this.configuracionCamara.direccion.perspectiva= false;
		this.camara.position.x = PROPIEDADES_CAMARA.POSICION_ARRIBA.X;
		this.camara.position.y = PROPIEDADES_CAMARA.POSICION_ARRIBA.Y;
		this.camara.position.z = PROPIEDADES_CAMARA.POSICION_ARRIBA.Z;
	}
	else{
		this.configuracionCamara.direccion.arriba=false;
		this.configuracionCamara.direccion.perspectiva= true;
		this.camara.position.x = PROPIEDADES_CAMARA.POSICION_PERSPECTIVA.X;
		this.camara.position.y = PROPIEDADES_CAMARA.POSICION_PERSPECTIVA.Y;
		this.camara.position.z = PROPIEDADES_CAMARA.POSICION_PERSPECTIVA.Z;
	}
	this.camara.lookAt(this.escenario.position);
}

//obtener la camara
Escena.prototype.getCamara = function(){
	return this.camara;
}

//configurar el renderer
Escena.prototype.obtenerRenderer = function(){
	return this.renderizador.domElement;
}

//configurar el aspecto
Escena.prototype.configurarAspecto = function(h, w){
	this.renderizador.setSize(w,h) ;
	this.camara.aspect = w/h;
	this.camara.updateProjectionMatrix();
}

//cambiar el foco de la camara
Escena.prototype.cambiarFoco = function(objeto3D){
   // this.camara.lookAt(objeto3D.contenedor.position);
   if(this.configuracionCamara.direccion.arriba ){
            this.camara.position.set(objeto3D.contenedor.position.x, this.camara.position.y, objeto3D.contenedor.position.z);   
            this.camara.lookAt(new THREE.Vector3(objeto3D.contenedor.position.x, objeto3D.contenedor.position.y, objeto3D.contenedor.position.z));
        }
        else if(this.configuracionCamara.direccion.perspectiva){
           this.camara.position.set(objeto3D.contenedor.position.x, objeto3D.contenedor.position.y + 10, objeto3D.contenedor.position.z - 70);  
            this.camara.lookAt(objeto3D.contenedor.position);
        }
}

//cambiar direccion de la camara
Escena.prototype.cambiarDireccionCamara = function(){
	if(this.configuracionCamara.direccion.arriba){
		this.configuracionCamara.direccion.arriba = false;
		this.configuracionCamara.direccion.perspectiva = true;
		this.camara.position.set(PROPIEDADES_CAMARA.POSICION_PERSPECTIVA.X, PROPIEDADES_CAMARA.POSICION_PERSPECTIVA.Y, PROPIEDADES_CAMARA.POSICION_PERSPECTIVA.Z);
		this.camara.lookAt(this.escenario.position);
		//this.cambiarFoco(this.escenario);
	}
	else if(this.configuracionCamara.direccion.perspectiva){
		this.configuracionCamara.direccion.arriba = true;
		this.configuracionCamara.direccion.perspectiva= false;
		this.camara.position.set(PROPIEDADES_CAMARA.POSICION_ARRIBA.X, PROPIEDADES_CAMARA.POSICION_ARRIBA.Y, PROPIEDADES_CAMARA.POSICION_ARRIBA.Z);
		this.camara.rotateOnAxis((new THREE.Vector3(1, 0, 0)).normalize(), (90 * Math.PI / 180));
        this.camara.up = new THREE.Vector3(0, 0, 1);
		//this.cambiarFoco(this.escenario);
		this.camara.lookAt(this.escenario.position);
	}
}

//nueva posicion de la camara
Escena.prototype.moverCamara = function (x, y ,z){
	this.camara.position.set(x,y,z);
	this.camara.lookAt(this.escenario);
}

//Frame para la animacion
Escena.prototype.renderizarEscena = function(){
	this.renderizador.render(this.escenario, this.camara);	
}

//agregar un objeto3D a la escena
Escena.prototype.agregarObjeto = function(objeto3D, posX, posY, posZ){
	posX = typeof posX == undefined ? 0 : posX;
	posY = typeof posY == undefined ? 0 : posY;
	posZ = typeof posZ == undefined ? 0 : posZ;
	//del objeto 3d hay que obtener su contenedor para agregarlo a la escena
	objeto3D.contenedor.position.set(posX, posY, posZ);
	this.escenario.add(objeto3D.getContenedor());
	
}

//cargar escena
Escena.prototype.cargarEscena = function(largo, ancho){
    var obj = ControladorEscena.cargarEscena(this.escenario, largo, ancho);
}

//agregar luz puntual
//luz = new THREE.SpotLight(color)
//color = html color #fff
//position = {x, y z}
Escena.prototype.agregarFuenteDeLuz = function(color, posicion){
	var nuevaLuz= new THREE.SpotLight(new THREE.Color(color));
	nuevaLuz.position.set(posicion.x, posicion.y, posicion.z);
	this.luces.fuentes.add(nuevaLuz);
	this.escenario.add(nuevaLuz);
}

//cambiar intensidad de la luz direccional
Escena.prototype.cambiarIntensidadLuzDireccional = function(intensidad){
	this.luces.direccional.intensity = intensidad;
}

//cambiar el color de la luz direccional
//color #html color
Escena.prototype.cambiarColorDireccional = function(color){
	this.luces.direccional.color = new THREE.Color(color);
}

//cambiar el color de la luz de ambiente
Escena.prototype.cambiarColorAmbiente = function(color){
	this.luces.ambiente.color = new THREE.Color(color);
}
