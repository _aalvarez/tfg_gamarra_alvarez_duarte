//un mundo muy basico
Mundo.seguirObjeto = true;
//archivo mundo es un arhivo json que tiene el objeto escena a dibujarse
function Mundo(archivoMundo, lienzo, alto, ancho){
	this.archivoMundo = archivoMundo;
	this.escena = new Escena();
	this.objetos = {};
	this.mapa = {};
	this.lienzo = lienzo;
	this.alto = alto;
	this.ancho= ancho;
}

//agregar un objeto al mundo
//objeto3D es un objeto de tipo Objeto3D
//posicion es un json con x, y z
Mundo.prototype.agregarObjeto = function(_objeto, _posicion){
	_objeto.setPosicion(_posicion);
	_objeto.setPosicionInicial(_posicion);
	var newId = this.obtenerNuevoIdParaObjeto();
	_objeto.setId(newId);
	this.objetos[_objeto.getId()] = _objeto;
	this.mapa[_objeto.getId()] = _posicion;
	this.escena.agregarObjeto(_objeto, _posicion.x, _posicion.y, _posicion.z);
}

//obtener un objeto del mundo
Mundo.prototype.obtenerObjeto = function(id){
	return this.objetos[id];
}
//dom element could be a div, figure, etc
Mundo.prototype.cargarEscena = function(){
	this.escena.inicializarEscena(this.archivoMundo, this.alto, this.ancho);
	this.lienzo.appendChild(this.escena.obtenerRenderer());
}

//lo que se va a ejecutar para la animacion del mundo
Mundo.prototype.frameDeAnimacion = function(){
	if(Mundo.seguirObjeto){
		for(var key in this.objetos){
			this.escena.cambiarFoco(this.objetos[key]);	
			break;
		}
	}
	this.escena.renderizarEscena();
}

//obtiene un id para el nuevo objeto agregado
Mundo.prototype.obtenerNuevoIdParaObjeto = function(){
	var count = 0;
	for(var key in this.objetos){
		count++;
	}
	return count+1;
}

//resetear escena

Mundo.prototype.resetearPosicionDeObjetos = function(){
	for(var key in this.objetos){
			this.objetos[key].reestablecerPosicion();	
		}
}