
'use strict';
Physijs.scripts.worker = '../../js/libraries/physijs/physijs_worker.js';
Physijs.scripts.ammo = '../../js/libraries/physijs/js/ammo.js';

function MITAI_APP(){

	//variables default para setear la camara y la escena
	var ANCHO =1300, ALTO=600;
	var ANGULO_VISION = 45, PLANO_CERCANO =0.1, PLANO_LEJANO=10000, RADIO_ASPECTO = ANCHO/ALTO;

	var _camara, _escena, _renderer;

	this.init =function(){

	}

	this.setearCamara = function(){
		_camara = new THREE.PerspectiveCamera(ANGULO_VISION, RADIO_ASPECTO,PLANO_CERCANO, PLANO_LEJANO);
	}
}
