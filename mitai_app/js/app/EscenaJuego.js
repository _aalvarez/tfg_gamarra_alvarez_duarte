//UNA ESCENA MUY MUY MUY MUUUUUUUY BASICA

var EscenaJuego =  Class.extend({
	init: function(nombreDelContenedor){
		'use strict'
		this.escena =  new Physijs.Scene();
		this.camara =  new THREE.PerspectiveCamera(45, 1,0.1,10000);
		this.luz_direccional =  new THREE.DirectionalLight(0xdfebff, 1.75);
		this.luz_ambiente =  new THREE.AmbientLight(new THREE.Color("#fff"));
		this.renderizador = new THREE.WebGLRenderer();
		this.contenedor = $('#'+nombreDelContenedor);
		this.objetos = {}
		this.personajePrincipal = null;
		this.setearAspecto();
		this.contenedor.append(this.renderizador.domElement);
		this.setearEscena();
		this.setearCamara();
		this.agregar('camara_principal', this.camara);
        this.isTopCamera = false;
        this.isNormalCamera = true;
		//this.setControls();
		//this.agregar('luz_direccional', this.luz_direccional);
		//this.agregar('luz_ambiente', this.luz_ambiente);
        this.comandos = {up: false, down:false, right: false, left: false};
	},
	setearAspecto: function(){
		'use strict'
		//tomamos las medidas del ancho y alto del contendor para setear el ancho y alto del renderizador
		//y el aspecto de vision de la camara
		var w = this.contenedor.width(),
		h = this.contenedor.height();
		this.renderizador.setSize(w,h) ;
		this.camara.aspect = w/h;
		this.camara.updateProjectionMatrix();
	},
	setearEscena: function(){
		'use strict'
		this.escena.setGravity(0, -50, 0); //setear la gravedad
	},
	setearCamara: function(){
		'use strict'
		this.camara.position.z = 300;
        this.camara.position.y=70;
        this.camara.position.y=100;
        this.camara.lookAt(this.escena.position);
	},
	agregar: function(objectName, objectToAdd){
		'use strict'
		this.objetos[objectName] = objectToAdd;
		this.dibujarObjetos();
	},
	eliminar: function(objectName){

	},
	dibujarObjetos: function(){
		for(var obj in this.objetos){
			this.escena.add(this.objetos[obj]);
		}
	},
	agregarPersonajePrincipal: function(personaje){
		personaje.mesh.position.x =85;
		personaje.mesh.position.y =5;
		personaje.mesh.position.z =20;
		personaje.mesh.scale.set( 0.05, 0.05, 0.05 );
		this.agregar("personaje_principal", personaje.mesh);
		this.personajePrincipal = personaje;
	},
	reiniciar: function () {
	    this.personajePrincipal.mesh.position.x = 85;
	    this.personajePrincipal.mesh.position.y = 5;
	    this.personajePrincipal.mesh.position.z = 20;

	    this.personajePrincipal.resetOrientacion();

	},
	// Event handlers
    setControls: function () {
        'use strict';
        // Within jQuery's methods, we won't be able to access "this"
        var user = this.personajePrincipal,
            // State of the different controls
            controls = {
                left: false,
                up: false,
                right: false,
                down: false
            };


        // When the user presses a key 
        jQuery(document).keydown(function (e) {
            var prevent = true;
            // Update the state of the attached control to "true"
            switch (e.keyCode) {
                case 37:
                    controls.left = true;
                    break;
                case 38:
                    controls.up = true;
                    break;
                case 39:
                    controls.right = true;
                    break;
                case 40:
                    controls.down = true;
                    break;
                default:
                    prevent = false;
            }
            // Avoid the browser to react unexpectedly
            if (prevent) {
                e.preventDefault();
            } else {
                return;
            }
            // Update the character's direction
            user.setDirection(controls);
        });
        // When the user releases a key
        jQuery(document).keyup(function (e) {
            var prevent = true;
            // Update the state of the attached control to "false"
            switch (e.keyCode) {
                case 37:
                    controls.left = false;
                    break;
                case 38:
                    controls.up = false;
                    break;
                case 39:
                    controls.right = false;
                    break;
                case 40:
                    controls.down = false;
                    break;
                default:
                    prevent = false;
            }
            // Avoid the browser to react unexpectedly
            if (prevent) {
                e.preventDefault();
            } else {
                return;
            }
            // Update the character's direction
            user.setDirection(controls);
        });
    },

    // Updating the camera to follow and look at a given Object3D / Mesh
    setFocus: function (object) {
        'use strict';
        if(this.isTopCamera){
            this.camara.position.set(object.position.x-70, this.camara.position.y, object.position.z+100);   
            this.camara.lookAt(new THREE.Vector3(object.position.x-70, object.position.y, object.position.z+100));
        }
        else if(this.isNormalCamera){
           this.camara.position.set(object.position.x, object.position.y + 40, object.position.z - 100);  
            this.camara.lookAt(object.position);
        }
    },
    camaraTop: function(){
        //declared once at the top of your code
        //var axis = new THREE.Vector3(0.5,0.5,0);//tilted a bit on x and y - feel free to plug your different axis here
        
        //this.camara.position.z=0;
        //this.camara.position.y=300;
        //this.camara.position.x=0;
        //this.camara.rotation.x=90;
        //this.camara.rotateOnAxis(axis,0.5);
        this.camara.position.y = 300;
        //this.camara.rotateOnAxis(new THREE.Vector3(0,0,0),0);
        this.camara.rotateOnAxis((new THREE.Vector3(1, 0, 0)).normalize(), (90 * Math.PI / 180));
        this.camara.up = new THREE.Vector3(0, 0, 1);
        this.isTopCamera =true;
        this.isNormalCamera=false;
    }
    ,
    camaraNormal: function(){
        this.camara.position.z = 300;
        this.camara.position.y=70;
        this.isTopCamera =false;
        this.isNormalCamera=true;
    }
    ,
	frame: function(){
		'use strict'
		//aca podemos llamar a una funcion para actualizar todos los personajes del juego
		//tambien seteamos la camar
		if(this.personajePrincipal !=null){
			//this.personajePrincipal.motion();
			this.setFocus(this.personajePrincipal.mesh);
		}
		this.escena.simulate();
		this.renderizador.render(this.escena, this.camara);	
	}
});
