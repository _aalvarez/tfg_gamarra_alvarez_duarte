var Character = Class.extend({
    // Class constructor
    init: function (args) {
        'use strict';
        // Set the different geometries composing the humanoid
        var head = new THREE.SphereGeometry(32, 8, 8),
            hand = new THREE.SphereGeometry(8, 4, 4),
            foot = new THREE.SphereGeometry(16, 4, 4, 0, Math.PI * 2, 0, Math.PI / 2),
            nose = new THREE.SphereGeometry(4, 4, 4),
            // Set the material, the "skin"
            material = new THREE.MeshNormalMaterial(args);
        // Set the character modelisation object
        this.mesh = new THREE.Object3D();
        this.mesh.position.y = 48;
        // Set and add its head
        this.head = new THREE.Mesh(head, material);
        this.head.position.y = 0;
        this.mesh.add(this.head);
        // Set and add its hands
        this.hands = {
            left: new THREE.Mesh(hand, material),
            right: new THREE.Mesh(hand, material)
        };
        this.hands.left.position.x = -40;
        this.hands.left.position.y = -8;
        this.hands.right.position.x = 40;
        this.hands.right.position.y = -8;
        this.mesh.add(this.hands.left);
        this.mesh.add(this.hands.right);
        // Set and add its feet
        this.feet = {
            left: new THREE.Mesh(foot, material),
            right: new THREE.Mesh(foot, material)
        };
        this.feet.left.position.x = -20;
        this.feet.left.position.y = -48;
        this.feet.left.rotation.y = Math.PI / 4;
        this.feet.right.position.x = 20;
        this.feet.right.position.y = -48;
        this.feet.right.rotation.y = Math.PI / 4;
        this.mesh.add(this.feet.left);
        this.mesh.add(this.feet.right);
        // Set and add its nose
        this.nose = new THREE.Mesh(nose, material);
        this.nose.position.y = 0;
        this.nose.position.z = 32;
        this.mesh.add(this.nose);
        // Set the vector of the current motion
        this.direction = new THREE.Vector3(0, 0, 0);
        // Set the current animation step
        this.step = 0;
        this.tween = null;
        this.orientacion = { NORTE: true, SUR: false, ESTE: false, OESTE: false };
        this.turns = { right: 1, left: -1 };
        this.lastTurn = 0;
        this.currentOrientation = { NORTE: true, SUR: false, ESTE: false, OESTE: false };
        this.rotationData = {
            right: {
                norte:
                    {
                          start: { y: 0 }, end: { y: -90 }
                      },
                    este:
                      {
                          start: { y: -90 }, end: { y: -180 }
                      },
                    sur:
                        {
                            start: { y: -180 }, end: { y: -270 }
                        },
                    oeste: {
                        start: { y: -270 }, end: { y: -360 }
                    }

                },
            left: {
                norte:
                      {
                          start: { y: 0 }, end: { y: 90 }
                      },
                oeste:
                  {
                      start: { y: 90 }, end: { y: 180 }
                  },
                sur:
                    {
                        start: { y: 180 }, end: { y: 270 }
                    },
                este: {
                    start: { y: 270 }, end: { y: 360 }
                }
            }
        };
    },
    resetRotationData: function () {
        //right
        this.rotationData.right.norte.start.y = 0;
        this.rotationData.right.norte.end.y = -90;

        this.rotationData.right.este.start.y = -90;
        this.rotationData.right.este.end.y = -180;

        this.rotationData.right.sur.start.y = -180;
        this.rotationData.right.sur.end.y = -270;

        this.rotationData.right.oeste.start.y = -270;
        this.rotationData.right.oeste.end.y = -360;

        //left
        this.rotationData.left.norte.start.y = 0;
        this.rotationData.left.norte.end.y = 90;

        this.rotationData.left.este.start.y = 270;
        this.rotationData.left.este.end.y = 360;

        this.rotationData.left.sur.start.y = 180;
        this.rotationData.left.sur.end.y = 270;

        this.rotationData.left.oeste.start.y = 90;
        this.rotationData.left.oeste.end.y = 180;

    },
    // Update the direction of the current motion
    setDirection: function (controls) {
        'use strict';
        // Either left or right, and either up or down (no jump or dive (on the Y axis), so far ...)
        var x = controls.left ? 1 : controls.right ? -1 : 0,
            y = 0,
            z = controls.up? 1 : controls.down ? -1 : 0;
        this.direction.set(x, y, z);
    },
    avanzar: function (steps) {
        var x = this.orientacion.OESTE ? steps : this.orientacion.ESTE ? -steps : 0;
        var y = 0;
        var z = this.orientacion.NORTE ? steps : this.orientacion.SUR ? -steps : 0;
        this.direction.set(x, y ,z);
        this.move();
        //alert("NORTE: " + this.orientacion.NORTE + " " + "SUR: " + this.orientacion.SUR + "ESTE: " + this.orientacion.ESTE + "OESTE: " + this.orientacion.OESTE);
    },
    girarDerecha: function (angle) {
        this.lastTurn = this.turns.right;
         this.rotate2(angle);
        //this.rotate();
    },
    girarIzquierda: function (angle) {
        this.lastTurn = this.turns.left;
        //this.rotate();
        this.rotate2(angle);
    },
    changeOrientation: function () {
        this.resetRotationData();
        if (this.lastTurn == this.turns.right) {
            if (this.orientacion.NORTE) {
                this.orientacion.NORTE = false;
                this.orientacion.ESTE = true;
            }
            else if (this.orientacion.ESTE) {
                this.orientacion.ESTE = false;
                this.orientacion.SUR = true;
            }
            else if (this.orientacion.SUR) {
                this.orientacion.SUR = false;
                this.orientacion.OESTE = true;
            }
            else if (this.orientacion.OESTE) {
                this.orientacion.OESTE = false;
                this.orientacion.NORTE = true;
                this.mesh.rotation.y = 0;
            }
        }

        else if (this.lastTurn == this.turns.left) {
            if (this.orientacion.NORTE) {
                this.orientacion.NORTE = false;
                this.orientacion.OESTE = true;
            }
            else if (this.orientacion.OESTE) {
                this.orientacion.OESTE = false;
                this.orientacion.SUR = true;
            }
            else if (this.orientacion.SUR) {
                this.orientacion.SUR = false;
                this.orientacion.ESTE = true;
            }
            else if (this.orientacion.ESTE) {
                this.orientacion.ESTE = false;
                this.orientacion.NORTE = true;
                this.mesh.rotation.y = 0;
                console.log("ROTATION.Y: " + this.mesh.rotation.y * (180 / Math.PI));
            }
        }
        console.log(this.orientacion);
    },
    // Process the character motions
    motion: function () {
        'use strict';
        // (if any)
        if (this.direction.x !== 0 || this.direction.z !== 0) {
            // Rotate the character
            this.rotate();
            // And, only if we're not colliding with an obstacle or a wall ...
            if (this.collide()) {
                return false;
            }
            // ... we move the character
            this.move();
            return true;
        }
    },
    walk: function(direction){
        if (direction.up == true) {
            this.direction.set(0, 0, 0.2);
        }
        else if (direction.down == true) {
            this.direction.set(0, 0, -0.2);
        }
        this.move();
    },
    // Rotate the character
    rotate: function () {
        'use strict';
        // Set the direction's angle, and the difference between it and our Object3D's current rotation
        var angle = Math.atan2(this.direction.x, this.direction.z),
            difference = angle - this.mesh.rotation.y;
        // If we're doing more than a 180°
        if (Math.abs(difference) > Math.PI) {
            // We proceed to a direct 360° rotation in the opposite way
            if (difference > 0) {
                this.mesh.rotation.y += 2 * Math.PI;
            } else {
                this.mesh.rotation.y -= 2 * Math.PI;
            }
            // And we set a new smarter (because shorter) difference
            difference = angle - this.mesh.rotation.y;
            // In short : we make sure not to turn "left" to go "right"
        }
        // Now if we haven't reached our target angle
        if (difference !== 0) {
            // We slightly get closer to it
            this.mesh.rotation.y += difference / 4;
        }
    },
    rotate2: function (angle) {
        var angleForRotation = angle;
       /* if (this.orientacion.OESTE) {
            angleForRotation += 90;
        }*/
        var angleRadians = angleForRotation * (Math.PI / 180);
        
        //console.log(angleRadians);
        //this.mesh.rotation.y += angleRadians;
        this.mesh.rotation.set(0, angleRadians, 0);
         //this.mesh.rotation.y = this.currentRotation;
        //var yAxis = new THREE.Vector3(0, 1, 0);
       // this.rotateAroundObjectAxis(this.mesh, yAxis, angleRadians);
        //console.log(this.mesh.rotation.y);
    },
    move: function () {
        'use strict';
        // We update our Object3D's position from our "direction"
        //console.log("step z: " + this.mesh.position.z);
        this.mesh.position.x += this.direction.x; //* ((this.direction.z === 0) ? 4 : Math.sqrt(8));
        this.mesh.position.z += this.direction.z; //* ((this.direction.x === 0) ? 4 : Math.sqrt(8));
        
        // Now let's use Sine and Cosine curves, using our "step" property ...
        this.step += 1 / 5;
        // ... to slightly move our feet and hands
        this.feet.left.position.setZ(Math.sin(this.step) * 16);
        this.feet.right.position.setZ(Math.cos(this.step + (Math.PI / 2)) * 16);
        this.hands.left.position.setZ(Math.cos(this.step + (Math.PI / 2)) * 8);
        this.hands.right.position.setZ(Math.sin(this.step) * 8);
    },
    collide: function () {
        'use strict';
        // INSERT SOME MAGIC HERE
        return false;
    },
    resetOrientacion: function () {
       /* if (this.orientacion.ESTE) {
            this.rotate2(90);
        }
        else if (this.orientacion.SUR) {
            this.rotate2(180);
        }
        else if (this.orientacion.OESTE) {
            this.rotate2(270);
        }*/
        this.orientacion = { NORTE: true, SUR: false, ESTE: false, OESTE: false };
        this.resetRotationData();
        this.mesh.y = 0;
    },
    getOrientation: function (turn) {


        if (turn == this.turns.right) {

            if (this.currentOrientation.NORTE) {
                this.currentOrientation.NORTE = false;
                this.currentOrientation.ESTE = true;
                result = this.rotationData.right.norte;
            }
            else if (this.currentOrientation.ESTE) {
                this.currentOrientation.ESTE = false;
                this.currentOrientation.SUR = true;
                result = this.rotationData.right.este;
            }
            else if (this.currentOrientation.SUR) {
                this.currentOrientation.SUR = false;
                this.currentOrientation.OESTE = true;
                result = this.rotationData.right.sur;
            }
            else if (this.currentOrientation.OESTE) {
                this.currentOrientation.OESTE = false;
                this.currentOrientation.NORTE = true;
                result = this.rotationData.right.oeste;
            }
        }

        else if (turn == this.turns.left) {
            if (this.currentOrientation.NORTE) {
                this.currentOrientation.NORTE = false;
                this.currentOrientation.OESTE = true;
                result = this.rotationData.left.norte;
            }
            else if (this.currentOrientation.ESTE) {
                this.currentOrientation.ESTE = false;
                this.currentOrientation.NORTE = true;
                result = this.rotationData.left.este;
            }
            else if (this.currentOrientation.SUR) {
                this.currentOrientation.SUR = false;
                this.currentOrientation.ESTE = true;
                result = this.rotationData.left.sur;
            }
            else if (this.currentOrientation.OESTE) {
                this.currentOrientation.OESTE = false;
                this.currentOrientation.SUR = true;
                result = this.rotationData.left.oeste;
            }
        }

        return result;
    }
});