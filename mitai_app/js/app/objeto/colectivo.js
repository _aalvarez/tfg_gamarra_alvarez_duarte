function Colectivo(){
	this.setContenedor();
	this.obtenerEstructura(this.contenedor);
	this.metodos = ['avanzar','girarDerecha','girarIzquierda', 'frenar'];
    this.largo = 14;
}

Colectivo.prototype = new Personaje();
Colectivo.prototype.constructor = Colectivo;


Colectivo.prototype.obtenerEstructura = function(target){
	var o_loader = new THREE.ObjectLoader();
	var this_escenario = this.escenario;
	o_loader.load('../../js/app/modelos/bus.json', function(obj){		
		//this_escenario.add(obj);
		//obj.scale.set(0.1,0.1,0.1);
		obj.position.set(0, 0, 0);
		target.add(obj);
		//alert(obj.position.x+' '+obj.position.y+' '+obj.position.z);
	});
}


//
Colectivo.prototype.avanzarAnimacion = function(){
    var largoCalle = ControladorEscena.largoCuadra;//largo de la calle a avanzar
    var avanzarCant = largoCalle-(this.largo+(this.largo/2));//cantidad a avanzar
    var orientacionInfo = this.getOrientacionData();//la orientacion del  autobus

    if(this.hayEsquina()){
        avanzarCant+= ControladorEscena.largoUnion+(this.largo+(this.largo/2));
    }

    //calculamos hacia que dirección va a avanzar el colectivo en x y z.
    var _x = orientacionInfo.oeste ? avanzarCant : orientacionInfo.este ? -avanzarCant : 0,
    _y = 0,
    _z = orientacionInfo.norte ? avanzarCant : orientacionInfo.sur ? -avanzarCant : 0;

    //simulamos que el colectivo avanza para calcular los valores de la animación
    this.posicionAnimacion.x += _x;
    this.posicionAnimacion.y += _y;
    this.posicionAnimacion.z += _z;

    //creamos el tween para animacion 
    var tween = new TWEEN.Tween({z:0}).to({z:avanzarCant}, 1500);
    var thisObjeto = this;
    var lastStep = 0;
    tween.onUpdate(function () {
        thisObjeto.avanzar(this.z - lastStep);
        lastStep = this.z;
    });
    return tween;
}

//
Colectivo.prototype.avanzar = function(steps){
    this.setDireccion(steps);
    this.contenedor.position.x += this.getDireccion().x;
    this.contenedor.position.z += this.getDireccion().z;    
}


////*////*//*/*/*/*/*/*/
Colectivo.prototype.avanzarGirando = function(cantX, cantZ){
    this.contenedor.position.x += cantX;
    this.contenedor.position.z += cantZ;
}

//
Colectivo.prototype.girarDerechaAnimacion = function(){

    var tweenData = this.getRotationData(Personaje.DERECHA);
    var tween = new TWEEN.Tween(tweenData.start).to(tweenData.end, 500);
    var thisObjeto = this;

     var lastZ = 0;
    var lastX = 0;

    //animacion
    this.posicionAnimacion.x += tweenData.end.x;
    this.posicionAnimacion.z += tweenData.end.z;


    tween.onUpdate(function () {
        ///escenaBasica.personajePrincipal.avanzar(this.z);
        //thisObjeto.avanzar(0.5);
        //thisObjeto.contenedor.position.z += 0.5;
        thisObjeto.avanzarGirando(this.x- lastX, this.z- lastZ);
        thisObjeto.girar(this.y);
        lastX = this.x;
        lastZ = this.z;
    }).onComplete(function(){
        thisObjeto.setOrientacion(Personaje.DERECHA);
    });
    thisObjeto.setOrientacionData(Personaje.DERECHA);
    return tween;
}

//
Colectivo.prototype.girarIzquierdaAnimacion = function(){
    var tweenData = this.getRotationData(Personaje.IZQUIERDA);
    var tween = new TWEEN.Tween(tweenData.start).to(tweenData.end, 1000);
    var thisObjeto = this;
    var lastZ = 0;
    var lastX = 0;

    //animacion
    this.posicionAnimacion.x += tweenData.end.x;
    this.posicionAnimacion.z += tweenData.end.z;


    tween.onUpdate(function () {
        //escenaBasica.personajePrincipal.avanzar(this.z);
        //thisObjeto.avanzar(0.5);
        thisObjeto.avanzarGirando(this.x- lastX, this.z- lastZ);
        thisObjeto.girar(this.y);
        lastX = this.x;
        lastZ = this.z;
    }).onComplete(function(){
        thisObjeto.setOrientacion(Personaje.IZQUIERDA);
    });
    thisObjeto.setOrientacionData(Personaje.IZQUIERDA);
    return tween;
}

//
Colectivo.prototype.girar = function(angle){
    var angleForRotation = angle;
    var angleRadians = angleForRotation * (Math.PI / 180);
    this.contenedor.rotation.set(0, angleRadians, 0);
}

Colectivo.prototype.frenar = function(){
    var tween = new TWEEN.Tween({x:0}).to({x:1}, 1000);
    var thisObjeto = this;
    tween.onUpdate(function () {
        //escenaBasica.personajePrincipal.avanzar(this.z);
    });
    return tween;
}

//
Colectivo.prototype.tieneMetodo = function(metodo){
    var result = false;
    for(var key in this.metodos){
        if(this.metodos[key]==metodo){
            result = true;
            break;
        }
    }
    return result;
}

//retorna true si el colectivo esta en una esquina
Colectivo.prototype.hayEsquina = function(){
    //console.log('x: '+Math.round(this.posicionAnimacion.x / ControladorEscena.longitudCuadro)+' z: '+Math.round(this.posicionAnimacion.z / ControladorEscena.longitudCuadro));

    var posCuadraX = Math.round(this.posicionAnimacion.x / ControladorEscena.longitudCuadro);
    var posCuadraZ = Math.round(this.posicionAnimacion.z / ControladorEscena.longitudCuadro);

    var posRelativaX = (posCuadraX*ControladorEscena.longitudCuadro) - this.posicionAnimacion.x;
    var posRelativaZ = (posCuadraZ*ControladorEscena.longitudCuadro) - this.posicionAnimacion.z;
    console.log('posRelX: '+ posRelativaX+', posRelZ: '+posRelativaZ);
    if(posRelativaZ==14 || posRelativaX==14){
        return true;
    }
    return false;
     
}

//
Colectivo.prototype.hayCalleAdelante = function(){
    var resultado = false;


    return resultado;
}

//
Colectivo.prototype.getRotationData = function(direccion){
        //Personaje.resetRotationData();
    switch(direccion){
        case Personaje.DERECHA:
            if(this.orientacionData.norte) return fnExtend(Colectivo.rotationData.right.norte);
            else if(this.orientacionData.este) return fnExtend(Colectivo.rotationData.right.este);
            else if(this.orientacionData.sur) return fnExtend(Colectivo.rotationData.right.sur);
            else if(this.orientacionData.oeste) return fnExtend(Colectivo.rotationData.right.oeste);
        break;
        case Personaje.IZQUIERDA:
            if(this.orientacionData.norte) return fnExtend(Colectivo.rotationData.left.norte);
            else if(this.orientacionData.este) return fnExtend(Colectivo.rotationData.left.este);
            else if(this.orientacionData.sur) return fnExtend(Colectivo.rotationData.left.sur);
            else if(this.orientacionData.oeste) return fnExtend(Colectivo.rotationData.left.oeste);
        break;
    }
}

Colectivo.rotationData = 
{
            right: {
                norte:
                    {
                          start: {x:0, y: 0 ,z:0}, end: {x:ControladorEscena.largoUnion+7, y: -90 ,z: (ControladorEscena.largoUnion+7)}
                      },
                    este:
                      {
                          start: {x:0, y: -90 ,z:0}, end: {x:-ControladorEscena.largoUnion, y: -180 ,z: -(ControladorEscena.largoUnion+7)}
                      },
                    sur:
                        {
                            start: {x:0, y: -180 ,z:0}, end: {x:-ControladorEscena.largoUnion, y: -270 ,z: -(ControladorEscena.largoUnion+7)}
                        },
                    oeste: {
                        start: {x:0, y: -270 ,z:0}, end: {x:ControladorEscena.largoUnion, y: -360 ,z: (ControladorEscena.largoUnion+7)}
                    }

                },
            left: {
                norte:
                      {
                          start: { x:0 , y: 0 , z:0 }, end: {x:ControladorEscena.largoUnion ,y: 90,z: ControladorEscena.largoUnion + 7 }
                      },
                oeste:
                  {
                      start: { x:0, y: 90, z:0 }, end: {x:ControladorEscena.largoUnion +7, y: 180, z: -(ControladorEscena.largoUnion)}
                  },
                sur:
                    {
                        start: {x:0 ,y: 180, z:0 }, end: {x:-(ControladorEscena.largoUnion+7) , y: 270, z: -(ControladorEscena.largoUnion) }
                    },
                este: {
                    start: {x:0 , y: 270, z:0 }, end: {x:ControladorEscena.largoUnion , y: 360, z: ControladorEscena.largoUnion + 7 }
                }
            }
}