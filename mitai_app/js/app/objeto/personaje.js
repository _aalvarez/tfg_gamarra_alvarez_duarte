//var _direccion = {derecha: 'DERECHA', izquierda: 'IZQUIERDA'}

Personaje.DERECHA = 'DERECHA';
Personaje.IZQUIERDA = 'IZQUIERDA';

//heredamos de Objeto
Personaje.prototype = new Objeto();
//creamos la nueva clase Personaje
function Personaje(){
	this.animacion = '';

	this.orientacion = { norte: true, sur: false, este: false, oeste: false };
	this.orientacionInicial = { norte: true, sur: false, este: false, oeste: false };
	this.orientacionData = {norte: true, sur:false, este:false, oeste: false};
	this.orientacionDataInicial  = {norte: true, sur:false, este:false, oeste: false};
	this.movimiento ='';
	this.direccion = new THREE.Vector3(0, 0, 0);
	this.setContenedor();//el contenedor del objeto3D;
	this.metodos =[];
	this.posicionAnimacion = fnExtend(this.posicionInicial);
}

Personaje.prototype.tieneMetodo = function(metodo){
    var result = false;
    for(var key in this.metodos){
        if(this.metodos[key]==metodo){
            result = true;
            break;
        }
    }
    return result;
}

Personaje.prototype.resetearTodo = function(){
	this.orientacion.norte = true;
	this.orientacion.sur = false;
	this.orientacion.este = false;
	this.orientacion.oeste = false;
}

//cambiamos el tipo de constructor del personaje
Personaje.prototype.constructor = new Personaje();


//animacion 
Personaje.prototype.setDireccion = function(steps){
	var x = this.orientacion.oeste ? steps : this.orientacion.este ? -steps : 0,
    y = 0,
    z = this.orientacion.norte ? steps : this.orientacion.sur ? -steps : 0;
    this.direccion.set(x, y, z);
}

Personaje.prototype.getDireccion = function(){
	return this.direccion;
}

//animacion
Personaje.prototype.setOrientacion = function(direccion){
	Personaje.resetRotationData();
	if(direccion==Personaje.DERECHA){
			if(this.orientacion.norte){
				this.orientacion.norte = false;
				this.orientacion.este = true;
			}
			else if(this.orientacion.este){
				this.orientacion.este = false;
				this.orientacion.sur = true;
			}
			else if(this.orientacion.sur){
				this.orientacion.sur= false;
				this.orientacion.oeste = true;
			}
			else if(this.orientacion.oeste){
				this.orientacion.oeste = false;
				this.orientacion.norte= true;
			}
	}
	else if(direccion== Personaje.IZQUIERDA){
			if(this.orientacion.norte){
				this.orientacion.norte=false;
				this.orientacion.oeste = true;
			}
			else if(this.orientacion.oeste){
				this.orientacion.oeste= false;
				this.orientacion.sur = true;
			}
			else if(this.orientacion.sur){
				this.orientacion.sur = false;
				this.orientacion.este = true;
			}
			else if(this.orientacion.este){
				this.orientacion.este=false;
				this.orientacion.norte=true;
			}
	}
}

//animacion
Personaje.prototype.setOrientacionData = function(direccion){
	Personaje.resetRotationData();
	if(direccion==Personaje.DERECHA){
			if(this.orientacionData.norte){
				this.orientacionData.norte = false;
				this.orientacionData.este = true;
			}
			else if(this.orientacionData.este){
				this.orientacionData.este = false;
				this.orientacionData.sur = true;
			}
			else if(this.orientacionData.sur){
				this.orientacionData.sur= false;
				this.orientacionData.oeste = true;
			}
			else if(this.orientacionData.oeste){
				this.orientacionData.oeste = false;
				this.orientacionData.norte= true;
			}
	}
	else if(direccion== Personaje.IZQUIERDA){
			if(this.orientacionData.norte){
				this.orientacionData.norte=false;
				this.orientacionData.oeste = true;
			}
			else if(this.orientacionData.oeste){
				this.orientacionData.oeste= false;
				this.orientacionData.sur = true;
			}
			else if(this.orientacionData.sur){
				this.orientacionData.sur = false;
				this.orientacionData.este = true;
			}
			else if(this.orientacionData.este){
				this.orientacionData.este=false;
				this.orientacionData.norte=true;
			}
	}
}

Personaje.prototype.getOrientacionData = function(){
	return this.orientacionData;
}

//get orientacion
Personaje.prototype.getOrientacion = function(){
	return this.orientacion;
}

Personaje.rotationData = 
{
            right: {
                norte:
                    {
                          start: { y: 0 }, end: { y: -90 }
                      },
                    este:
                      {
                          start: { y: -90 }, end: { y: -180 }
                      },
                    sur:
                        {
                            start: { y: -180 }, end: { y: -270 }
                        },
                    oeste: {
                        start: { y: -270 }, end: { y: -360 }
                    }

                },
            left: {
                norte:
                      {
                          start: { y: 0 }, end: { y: 90 }
                      },
                oeste:
                  {
                      start: { y: 90 }, end: { y: 180 }
                  },
                sur:
                    {
                        start: { y: 180 }, end: { y: 270 }
                    },
                este: {
                    start: { y: 270 }, end: { y: 360 }
                }
            }
}

Personaje.resetRotationData = function(){
		Personaje.rotationData.right.norte.start.y = 0;
        Personaje.rotationData.right.norte.end.y = -90;

        Personaje.rotationData.right.este.start.y = -90;
        Personaje.rotationData.right.este.end.y = -180;

        Personaje.rotationData.right.sur.start.y = -180;
        Personaje.rotationData.right.sur.end.y = -270;

        Personaje.rotationData.right.oeste.start.y = -270;
        Personaje.rotationData.right.oeste.end.y = -360;

        //left
        Personaje.rotationData.left.norte.start.y = 0;
        Personaje.rotationData.left.norte.end.y = 90;

        Personaje.rotationData.left.este.start.y = 270;
        Personaje.rotationData.left.este.end.y = 360;

        Personaje.rotationData.left.sur.start.y = 180;
        Personaje.rotationData.left.sur.end.y = 270;

        Personaje.rotationData.left.oeste.start.y = 90;
        Personaje.rotationData.left.oeste.end.y = 180;
}

Personaje.prototype.getRotationData = function(direccion){
	//Personaje.resetRotationData();
	switch(direccion){
		case Personaje.DERECHA:
			if(this.orientacionData.norte) return Personaje.rotationData.right.norte;
			else if(this.orientacionData.este) return Personaje.rotationData.right.este;
			else if(this.orientacionData.sur) return Personaje.rotationData.right.sur;
			else if(this.orientacionData.oeste) return Personaje.rotationData.right.oeste;
		break;
		case Personaje.IZQUIERDA:
			if(this.orientacionData.norte) return Personaje.rotationData.left.norte;
			else if(this.orientacionData.este) return Personaje.rotationData.left.este;
			else if(this.orientacionData.sur) return Personaje.rotationData.left.sur;
			else if(this.orientacionData.oeste) return Personaje.rotationData.left.oeste;
		break;
	}
}


Personaje.prototype.setOrientacionInicial = function(orientacion){
	this.orientacionInicial = orientacion;
}

Personaje.prototype.setOrientacionDataInicial = function(orientacion){
	this.orientacionDataInicial = orientacion;
}

Personaje.prototype.getOrientacionInicial = function(){
	return this.orientacionInicial;
}

Personaje.prototype.getOrientacionDataInicial = function(){
	return this.orientacionDataInicial;
}

Personaje.prototype.setPosicionAnimacion = function(pos){
	this.posicionAnimacion = fnExtend(pos);
}

Personaje.prototype.reestablecerPosicion = function(){
	this.setPosicion(this.getPosicionInicial());
	this.setPosicionAnimacion(this.getPosicionInicial());
	this.contenedor.rotation.y =0;
	this.orientacion = { norte: true, sur: false, este: false, oeste: false };
	this.orientacionData = { norte: true, sur: false, este: false, oeste: false };
}