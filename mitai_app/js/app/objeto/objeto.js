//objeto que puede ser agregado al mundo, se necesita de la librería trhee.js
//para cargar el THREE.Object3D();

function Objeto(){
	this.id = -1;
	this.nombre = '';
	this.contenedor = undefined;
	this.posicion ={x:0, y:0, z:0};
	this.posicionInicial = fnExtend(this.posicion);
	this.setContenedor();
}



Objeto.prototype.setContenedor = function(){
	this.contenedor = new THREE.Object3D();
}

//metodo para obtener el contenedor del objeto 3D
Objeto.prototype.getContenedor = function(){
	return this.contenedor;
}

//metodo para obtener todos los metodos del objeto a ser ejecutados
Objeto.prototype.getMetodos = function(){
	return this.metodos;
}

//metodo para obtener la posicion del objeto
Objeto.prototype.getPosicion = function(){
	return this.posicion;
}

//metodo para obtener el id del Objeto
Objeto.prototype.getId = function(){
	return this.id;
}

//metodo para obtener el nombre del Objeto
Objeto.prototype.getNombre = function(){
	return this.nombre;
}

//cambiar el nombre
Objeto.prototype.setNombre = function(nombre){
	this.nombre = nombre;
}

//cambiar la posicion
Objeto.prototype.setPosicion = function(posicion){
	this.posicion = posicion;
	this.contenedor.position.set(posicion.x, posicion.y, posicion.z);
}

//set id
Objeto.prototype.setId = function(newId){
	this.id = newId;
}

Objeto.prototype.agregarAlContenedor = function(obj){
	this.contenedor.add(obj);
}

Objeto.prototype.getPosicionInicial = function(){
	return this.posicionInicial;
}

Objeto.prototype.setPosicionInicial = function(posicionInicial){
	this.posicionInicial = posicionInicial;
}
