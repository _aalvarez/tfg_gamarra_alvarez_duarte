//el elemento dom en donde va a vivir el editor de bloques
function Programador(htmlDom){
	this.htmlDom = htmlDom;
	this.menuXml = cons_XML ;// el menú que se usa en el editor de blockly
	this.espacioProgramacion = undefined; 
	this.objetoActual = undefined; //Ezal objeto que se está programando
	this.codigoGenerado = ''; //El código generado al traducir los bloques de blockly a código java
}

//
Programador.prototype.inicializar = function(){
	this.espacioProgramacion = Blockly.inject(this.htmlDom, { toolbox: this.menuXml}); //el workspace de blockly donde se ubicarán los bloques
}

//setea el objeto actual que se está programando
Programador.prototype.setObjetoActual = function(objeto3D){
	this.objetoActual = objeto3D;
}

//obtener el codigo actual del editor
Programador.prototype.getCodigo = function(){
	this.codigoGenerado = Blockly.JavaScript.workspaceToCode(this.espacioProgramacion);
	return this.codigoGenerado;
}

//cambiar el tool box del editor
Programador.prototype.cambiarMenu = function(nuevoMenuXML){
	this.espacioProgramacion.updateToolbox(nuevoMenuXML);
}

//get objeto actual
Programador.prototype.getObjetoActual = function(){
	return this.objetoActual;
}

//xml del menu
var cons_XML= '<xml>';
cons_XML +=  '<category name="Inicio">';
cons_XML +=         '<block type="start"></block>';
cons_XML +=   '</category>';
cons_XML +=  '<category name="Comandos">';
cons_XML +=         '<block type="avanzar"></block>';
cons_XML += 		'<block type="girarIzquierda"></block>';
cons_XML += 		'<block type="girarDerecha"></block>';
cons_XML +=			'<block type="frenar"></block>';
cons_XML +=   '</category>';
cons_XML +=     '<category name="Control">';
cons_XML +=         '<block type="bucleFor"></block>';
cons_XML += 		'<block type="condicionalif"></block>';
cons_XML += 		'<block type="bucleWhile"></block>';
cons_XML +=   '</category>';
cons_XML +=     '<category name="Sonidos">';
cons_XML +=   '</category>';
cons_XML += '<category name="Personajes">';
cons_XML += '</category>';
cons_XML +=   '<category name="Senales">';
cons_XML +=         '<block type="logica_pare"></block>';
cons_XML += 		'<block type="logica_semaforoRojo"></block>';
cons_XML += 		'<block type="logica_semaforoNaranja"></block>';
cons_XML += 		'<block type="logica_semaforoVerde"></block>';
cons_XML +=   '</category>';
cons_XML += '</xml>';