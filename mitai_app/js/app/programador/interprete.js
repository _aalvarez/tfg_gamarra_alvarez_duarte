Interprete.SUCCESS = true;
Interprete.ERROR = false;
Interprete.arraySentencias = new Array();

function Interprete(){
	this.objeto = undefined;
	this.sentenciaPorObjeto = new Array();
	this.resultado = Interprete.SUCCESS;
}

Interprete.prototype.inicializar = function(interpreter, scope){
	var wrapper;
	  wrapper = function(id) {
	    //Maze.move(0, id.toString());
	    Interprete.arraySentencias.push('avanzar');
	  };
	  interpreter.setProperty(scope, 'avanzar', interpreter.createNativeFunction(wrapper));

	  wrapper = function(id) {
	    //Maze.move(0, id.toString());
	    Interprete.arraySentencias.push('girarDerecha');
	  };
	  interpreter.setProperty(scope, 'girarDerecha', interpreter.createNativeFunction(wrapper));

	  wrapper = function(id) {
	    //Maze.move(0, id.toString());
	    Interprete.arraySentencias.push('girarIzquierda');
	  };
	  interpreter.setProperty(scope, 'girarIzquierda', interpreter.createNativeFunction(wrapper));

	  wrapper = function(id) {
	    //Maze.move(0, id.toString());
	    Interprete.arraySentencias.push('frenar');
	  };
	  interpreter.setProperty(scope, 'frenar', interpreter.createNativeFunction(wrapper));
}

/*
aca se ven que se va a ejecutar una vez que el codigo sea interpretado, es decir aca ya se va a saber como va a reaccionar el objeto programado
ya se va a saber cual es el resultado final del codigo escrito por el usuario
*/
Interprete.prototype.ejecutarCodigo = function(codigo){
	this.sentenciaPorObjeto.splice(0, this.sentenciaPorObjeto.length);
	Interprete.arraySentencias.splice(0, Interprete.arraySentencias.length);
	var arraySentencias = this.sentenciaPorObjeto;
	//eval(codigo);
	var myInterpreter = new Interpreter(codigo, this.inicializar);
	myInterpreter.run();
	this.sentenciaPorObjeto = Interprete.arraySentencias;

	function avanzar(){
		arraySentencias.push('avanzar');
	}

	function girarDerecha(){
		arraySentencias.push('girarDerecha');
	}

	function girarIzquierda(){
		arraySentencias.push('girarIzquierda');
	}

	function hayPare(){
		arraySentencias.push('hayPare');
		return true;
	}
	function frenar(){
		arraySentencias.push('frenar');
	}
}

Interprete.prototype.getCodigoInterpretado = function(){
	/*var codigoInterpretado =  '';
	for(var index in this.sentenciaPorObjeto){
		codigoInterpretado += this.objeto.getNombre()+'.'+this.sentenciaPorObjeto[index]+';\n';
	}*/
	//return this.sentenciaPorObjeto;//codigoInterpretado;
	return Interprete.arraySentencias;
}

Interprete.prototype.setObjeto = function(obj){
	this.objeto = obj;
}

Interprete.prototype.configurarAnimador = function(animador){
	animador.vaciar();
	for(var index in this.sentenciaPorObjeto){
		var sentencia = this.sentenciaPorObjeto[index];
		animador.agregarAnimacion(this.objeto, sentencia);
	}
}
