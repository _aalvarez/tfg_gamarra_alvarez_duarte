Vista.seguirAlObjeto = true;

function Vista(){
    this.controlador = new Controlador();
    this.pantallaPrincipal = '';
}
//inicializa toda la pantalla
Vista.prototype.inicializar = function(){
    this.setMundo();
    this.setProgramador();
    this.controlador.inicializar();
    this.setBlockly();
}

//resetear la escena
Vista.prototype.setAspectoEscena = function(h,w){
    this.controlador.mundo.escena.configurarAspecto(h,w);
}

//obtiene el elemento del dom donde esta la escena
Vista.prototype.getEscenaDom = function(){
    return this.pantallaPrincipal;
}

//setea el elemento dle dom donde irá la escena
Vista.prototype.setEscenaDom = function(elementoDom){
    this.pantallaPrincipal = elementoDom;
}

//inicializa blockly
Vista.prototype.setBlockly = function(){
    var urls = 
    { 
        'menu1': '../../img/menu/inicio.png', 
        'menu2': '../../img/menu/comandos.png', 
        'menu3': '../../img/menu/control.png', 
        'menu4': '../../img/menu/sonido.png', 
        'menu5': '../../img/menu/personaje.png', 
        'menu6': '../../img/menu/senhales.png' 
    }

    $(".blocklyTreeRoot .blocklyTreeRow").prepend("<div><img class='menu_icon text-center' /></div>");
    $(".blocklyTreeRoot #\\:1 .blocklyTreeRow .menu_icon").attr('src', urls.menu1);
    $(".blocklyTreeRoot #\\:2 .blocklyTreeRow .menu_icon").attr('src', urls.menu2);
    $(".blocklyTreeRoot #\\:3 .blocklyTreeRow .menu_icon").attr('src', urls.menu3);
    $(".blocklyTreeRoot #\\:4 .blocklyTreeRow .menu_icon").attr('src', urls.menu4);
    $(".blocklyTreeRoot #\\:5 .blocklyTreeRow .menu_icon").attr('src', urls.menu5);
    $(".blocklyTreeRoot #\\:6 .blocklyTreeRow .menu_icon").attr('src', urls.menu6);
}

//inicializa el mundo
Vista.prototype.setMundo = function(){
    var h = $(this.pantallaPrincipal).height();
    var w = $(this.pantallaPrincipal).width();
    var mundo = new Mundo("scene_dos.json", this.pantallaPrincipal, h, w );
    this.controlador.setMundo(mundo);
}

//inicializar el programador
Vista.prototype.setProgramador = function(){
    var blocklyDiv = document.getElementById('blocklyEditor');
    var programador = new Programador(blocklyDiv);
    this.controlador.setProgramador(programador);
}


//agregar un nuevo personaje a la escena
Vista.prototype.agregarObjeto = function(objeto, posicion){
    this.controlador.mundo.agregarObjeto(objeto, posicion);
}