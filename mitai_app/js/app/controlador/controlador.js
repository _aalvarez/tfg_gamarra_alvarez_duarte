function Controlador(){
	this.mundo = undefined;
	this.programador = undefined;
	this.animador = new Animador();
	this.interprete = new Interprete();

}

Controlador.prototype.inicializar = function(){
	this.mundo.cargarEscena();
	this.programador.inicializar();
}

Controlador.prototype.setMundo = function(mundo){
	this.mundo = mundo;
}

Controlador.prototype.setProgramador = function(programador){
	this.programador = programador;
}

Controlador.prototype.setAnimador = function(){
	this.interprete.setAnimador(this.animador);
}
