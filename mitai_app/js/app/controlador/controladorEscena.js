var ControladorEscena = ControladorEscena || {}

ControladorEscena.UP = '_up';
ControladorEscena.DOWN = '_down';
ControladorEscena.LEFT = '_left';
ControladorEscena.RIGHT = '_right';




ControladorEscena.maxWidth = 0;
ControladorEscena.maxHeight = 0;
ControladorEscena.xValue = 23.6;
ControladorEscena.zValue = 23.6;
ControladorEscena.longitudCuadro =  141.6;
ControladorEscena.largoCuadra = 118;
ControladorEscena.largoUnion = 23.6;


ControladorEscena.cuadra = undefined;
ControladorEscena.union = undefined;
ControladorEscena.asfalto_right = undefined;
ControladorEscena.asfalto_left = undefined;
ControladorEscena.asfalto_top = undefined;
ControladorEscena.asfalto_bottom = undefined;

//target tiene que ser un objeto 3d de three.js
//carga las cuadras con las calles y mapea toda la escena
ControladorEscena.cargarEscena = function(_target, _h, _w){

	var escena = new THREE.Object3D();

	ControladorEscena.ConfigurarSkyBox(escena);

	//para las cuadras
	var _width = _h;
	var _height= _w;
	var _x = ControladorEscena.xValue;
	var _y = 0.0;
	var _valorDeIncremento = ControladorEscena.longitudCuadro;
	ControladorEscena.maxHeight = _h * ControladorEscena.longitudCuadro;
	ControladorEscena.maxWidth = _w * ControladorEscena.longitudCuadro;


	//ahora dibujamos las cuadras en la escena

	for(var i=0; i<_width; i++){
		var _z = ControladorEscena.zValue;
		for(var j=0; j<_height; j++){
			ControladorEscena.cargarCuadra(escena, {x: _x, y: _y, z:_z});
			//ControladorEscena.cargarCasa(escena, {x: _x, y:_y, z:_z});
			_z+=_valorDeIncremento;	
		}
		_x += _valorDeIncremento;
	}

	//y ahora dibujamos las calles
	var _x1 =0, _x2=ControladorEscena.xValue,_x3=0;

	for(var i=0; i<=_width; i++){
		var _z1 =0, _z2=0, _z3=ControladorEscena.zValue;

		for(var j=0; j<=_height; j++){
			ControladorEscena.cargarUnion(escena, {x: _x1, y: _y, z: _z1});
			if(i!= _width){
				if(j%2==0){
					ControladorEscena.cargarAsfalto(escena, {x:_x2, y: _y, z:_z2}, ControladorEscena.LEFT);	
				}
				else{
					ControladorEscena.cargarAsfalto(escena, {x:_x2, y: _y, z:_z2}, ControladorEscena.RIGHT);
				}
				
			}
			if(j!= _height){
				if(i%2 == 0){
					ControladorEscena.cargarAsfalto(escena, {x:_x3, y: _y, z:_z3}, ControladorEscena.DOWN);		
				}
				else{
					ControladorEscena.cargarAsfalto(escena, {x:_x3, y: _y, z:_z3}, ControladorEscena.UP);			
				}
				
			}
			_z1+= _valorDeIncremento;
			_z2+= _valorDeIncremento;
			_z3+= _valorDeIncremento;
		}
		_x1 += _valorDeIncremento;
		_x2 += _valorDeIncremento;
		_x3 += _valorDeIncremento;
	}

	//segun el nivel vemos donde ubicar las señales y los semaforos



	_target.add(escena)
}


//agregar skybox a la escena
ControladorEscena.ConfigurarSkyBox = function(target){

	var imgUrls = ['../../img/skybox/left.png','../../img/skybox/right.png','../../img/skybox/top.png','../../img/skybox/bottom.png','../../img/skybox/rear.png','../../img/skybox/front.png'];

	var texturas = THREE.ImageUtils.loadTextureCube(imgUrls);
	texturas.format = THREE.RGBFormat;

	var geometriaDelSkyBox = new THREE.CubeGeometry(2000, 2000, 2000);
	var materialDelSkyBox = new THREE.MeshBasicMaterial({ color: new THREE.Color('#00aad4'), side: THREE.BackSide });
	/*var skyShader = THREE.ShaderLib["cube"];
    skyShader.uniforms["tCube"].value = texturas;

     var skyMaterial = new THREE.ShaderMaterial( {
      fragmentShader: skyShader.fragmentShader, vertexShader: skyShader.vertexShader,
      uniforms: skyShader.uniforms, depthWrite: false, side: THREE.BackSide
     });
*/

	var skybox = new THREE.Mesh(geometriaDelSkyBox, materialDelSkyBox);
	//skyMaterial.needsUpdate = true;
 	skybox.position.set(0,0,0);
	target.add(skybox);

}


ControladorEscena.getMapaEscena = function(h,w){
	var _width = _h;
	var _height= _w;
	var _x = ControladorEscena.xValue;
	var _y = 0.0;
	var _valorDeIncremento = controladorEscena.longitudCuadro;

	//Primero mapeamos toda la escena guardando la información sobre donde van a dibujarse
	//las cuadras y eso, con sus respectivas esquinas

	//mapa de la escena
	var mapaEscena = new Array(_width);
	var largoCuadra = controladorEscena.largoCuadra;
	var largoCalle = controladorEscena.longitudCuadro;
	for(var i=0; i<_width; i++){
		var _z = ControladorEscena.zValue;
		var mapaCuadra = new Array(_height);

		for(var j=0; j<_height; j++){
			var cuadra = {
				id: i+''+j,
				pos:{x: _x, y: _y, z:_z},
				esquinas: {
					uno:{x:_x, y:0, z:_z, signos: null},
					dos:{x:_x+largoCuadra, y:0, z:_z, signos: null},
					tres:{x:_x+largoCuadra, y:0, z:_z+largoCuadra, signos: null},
					cuatro:{x:_x, y:0, z:_z+largoCuadra, signos: null}
				}
				/*,
				calles{
					uno:{x:0, y:0, z:0, sentido: null},
					dos:{x:0, y:0, z:0, sentido: null},
					tres:{x:0, y:0, z:0, sentido: null},
					cuatro:{x:0, y:0, z:0, sentido: null}
				}*/
			}

			mapaCuadra[j]= cuadra;
			_z+=_valorDeIncremento;	
		}
		mapaEscena[i] = mapaCuadra;
		_x += _valorDeIncremento;
	}
	return mapaEscena;

}

//carga una cuadra
ControladorEscena.cargarCuadra = function(target, posicion){
	var o_loader = new THREE.ObjectLoader();
	var this_escenario = this.escenario;
	if(ControladorEscena.cuadra==undefined){
		o_loader.load('../../js/app/modelos/cuadra_casa.json', function(obj){		
			//this_escenario.add(obj);
			//obj.scale.set(0.1,0.1,0.1);
			ControladorEscena.cuadra = obj;
			obj.position.set(posicion.x, posicion.y, posicion.z);
			target.add(obj);
			//alert(obj.position.x+' '+obj.position.y+' '+obj.position.z);
		});
	}
	else{
		alert('ya no es undefined');
		ControladorEscena.cuadra.position.set(posicion.x, posicion.y, posicion.z);
		target.add(ControladorEscena.cuadra) ;
	}

	
}

//cargar el asfalto para la escena
//dir: 'up'. 'down', 'left', right
ControladorEscena.cargarAsfalto = function(target, posicion, dir){
	var o_loader = new THREE.ObjectLoader();
	var this_escenario = this.escenario;
	var modelname = '../../js/app/modelos/asfalto'+dir+'.json'
	o_loader.load(modelname, function(obj){		
		//this_escenario.add(obj);
		obj.position.set(posicion.x, posicion.y, posicion.z);
		target.add(obj);
		//alert(obj.position.x+' '+obj.position.y+' '+obj.position.z);
	});
}

//cargar la union para las cuadras
ControladorEscena.cargarUnion = function(target, posicion){
	var o_loader = new THREE.ObjectLoader();
	var this_escenario = this.escenario;
	o_loader.load('../../js/app/modelos/union.json', function(obj){		
		//this_escenario.add(obj);
		obj.position.set(posicion.x, posicion.y, posicion.z);
		target.add(obj);
		//alert(obj.position.x+' '+obj.position.y+' '+obj.position.z);
	});
}

//Cargar la casita

ControladorEscena.cargarCasa = function(target, posicion){
	var o_loader = new THREE.ObjectLoader();
	var this_escenario = this.escenario;
	o_loader.load('../../js/app/modelos/arbol.json', function(obj){		
		//this_escenario.add(obj);
		obj.position.set(posicion.x, posicion.y, posicion.z);
		target.add(obj);
		//alert(obj.position.x+' '+obj.position.y+' '+obj.position.z);
	});
}