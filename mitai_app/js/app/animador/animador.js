// este animador requiere la librería tweens
function Animador(){
	this.tweens = [];
}

//agrega un nuevo tween de animacion
Animador.prototype.agregarAnimacion = function(objeto_a_animar, metodo_a_ejecutar){

	
	if(objeto_a_animar.tieneMetodo(metodo_a_ejecutar)){
		if(metodo_a_ejecutar == 'avanzar'){
			var newTween = objeto_a_animar.avanzarAnimacion();
		    this.tweens.push(newTween);		
		    
		    console.log('posX: '+objeto_a_animar.posicionAnimacion.x+', posZ: '+objeto_a_animar.posicionAnimacion.z);
		    //console.log('x: '+Math.round(objeto_a_animar.posicionAnimacion.x / ControladorEscena.longitudCuadro)+' z: '+Math.round(objeto_a_animar.posicionAnimacion.z / ControladorEscena.longitudCuadro));
		}
		else if(metodo_a_ejecutar == 'girarDerecha'){
			if(objeto_a_animar.hayEsquina()){
				var newTween = objeto_a_animar.girarDerechaAnimacion();
		    	this.tweens.push(newTween);		
		    }
		}
		else if(metodo_a_ejecutar == 'girarIzquierda'){
			if(objeto_a_animar.hayEsquina()){
				var newTween = objeto_a_animar.girarIzquierdaAnimacion();
				this.tweens.push(newTween);	
			}
			
		}
		else if(metodo_a_ejecutar == 'hayPare'){
			var newTween = new TWEEN.Tween({time:0}).to({time:1}, 1);
			newTween.onUpdate(function () {
		        //escenaBasica.personajePrincipal.avanzar(this.z);
		        //thisObjeto.girar(this.y);
		        //alert(objeto_a_animar.getPosicion().x+' '+objeto_a_animar.getPosicion().y+' '+objeto_a_animar.getPosicion().z);
		    });
		    this.tweens.push(newTween);

		}
		else if(metodo_a_ejecutar == 'frenar'){
			var newTween = objeto_a_animar.frenar();
			this.tweens.push(newTween);
		}
	}
	
}

//Empezar a animar
Animador.prototype.iniciarAnimacion = function(){
	for(var i=0; i< this.tweens.length; i++){
		if (1==this.tweens.length){
		  this.tweens[0].start();
		}
		else{
			var nextTween =  i+1;
			if(nextTween == this.tweens.length){
			    this.tweens[0].start();
			}
			else{
			  this.tweens[i].chain(this.tweens[nextTween]);
			}
		}
	}
}

//vaciar animador 

Animador.prototype.vaciar = function(){
	this.tweens.splice(0, this.tweens.length);
}

