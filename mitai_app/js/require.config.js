requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: '../../js',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        threejs: 'libraries/three_js/three.min',
        physijs: 'libraries/physijs/physi',
        escena: 'app/escena/escena',
        mitai: 'app/escena/mundo'
    },
    shim: {
        mitai:[
            "threejs",
            "physijs",
            "escena"
        ]
    }
});

// Start the main app logic
