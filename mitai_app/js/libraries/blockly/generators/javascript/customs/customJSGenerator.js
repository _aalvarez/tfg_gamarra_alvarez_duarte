
//generador de codigo para avanzar
Blockly.JavaScript['avanzar'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'avanzar();\n';
  return code;
};

//generador de codigo para el bucle for
Blockly.JavaScript['bucleFor'] = function(block) {
  // Repeat n times (internal number).
  var repeats = Number(block.getFieldValue('repeticiones'));
  var branch = Blockly.JavaScript.statementToCode(block, 'hacer');
  branch = Blockly.JavaScript.addLoopTrap(branch, block.id);
  var loopVar = Blockly.JavaScript.variableDB_.getDistinctName(
      'contador', Blockly.Variables.NAME_TYPE);
  var code = 'for (var ' + loopVar + ' = 0; ' +
      loopVar + ' < ' + repeats + '; ' +
      loopVar + '++) {\n' +
      branch + '}\n';
  return code;
};


//generador de codigo para el bucle while
Blockly.JavaScript['bucleWhile'] = function(block) {
  // Do while/until loop.
  var until = block.getFieldValue('MODE') == 'UNTIL';
  var argument0 = Blockly.JavaScript.valueToCode(block, 'CONDICION', Blockly.JavaScript.ORDER_NONE) || 'false';
  var branch = Blockly.JavaScript.statementToCode(block, 'HACER');
  branch = Blockly.JavaScript.addLoopTrap(branch, block.id);
  if (until) {
    argument0 = '!' + argument0;
  }
  return 'while (' + argument0 + ') {\n' + branch + '}\n';
};

//generador de codigo para el condicional if

Blockly.JavaScript['condicionalif'] = function(block) {
  // If/elseif/else condition.
  var n = 0;
  var argument = Blockly.JavaScript.valueToCode(block, 'IF' + n,
      Blockly.JavaScript.ORDER_NONE) || 'false';
  var branch = Blockly.JavaScript.statementToCode(block, 'DO' + n);
  var code = 'if (' + argument + ') {\n' + branch + '}';
  for (n = 1; n <= block.elseifCount_; n++) {
    argument = Blockly.JavaScript.valueToCode(block, 'IF' + n,
        Blockly.JavaScript.ORDER_NONE) || 'false';
    branch = Blockly.JavaScript.statementToCode(block, 'DO' + n);
    code += ' else if (' + argument + ') {\n' + branch + '}';
  }
  if (block.elseCount_) {
    branch = Blockly.JavaScript.statementToCode(block, 'ELSE');
    code += ' else {\n' + branch + '}';
  }
  return code + '\n';
};


//generador de codigo para girar derecha
Blockly.JavaScript['girarDerecha'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'girarDerecha();\n';
  return code;
};


//generador de codigo para girar izquierda
Blockly.JavaScript['girarIzquierda'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'girarIzquierda();\n';
  return code;
};


//generador de codigo para la logica pare
Blockly.JavaScript['logica_pare'] = function(block) {
   var code = 'hayPare()';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];;
};

//generador de codigo para semaforo
Blockly.JavaScript['logica_semaforoRojo'] = function(block) {
   var code = 'haySemaforoRojo()';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];;
};


//generador de codigo para semaforo naranja

Blockly.JavaScript['logica_semaforoNaranja'] = function(block) {
   var code = 'haySemaforoNaranja()';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];;
};

//generador de codigo para semaforo rojo
Blockly.JavaScript['logica_semaforoRojo'] = function(block) {
   var code = 'haySemaforoRojo()';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];;
};

//generador de codigo para semaforo verde
Blockly.JavaScript['logica_semaforoVerde'] = function(block) {
   var code = 'haySemaforoVerde()';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];;
};


//bloque dentro del cual va el código que se va a ejecutar al hacer click sobre el boton iniciar juego
Blockly.JavaScript['start'] = function(block) {
  var statements_start = Blockly.JavaScript.statementToCode(block, 'start');
  // TODO: Assemble JavaScript into code variable.

  var code = 'function iniciar(){\n' +statements_start+'}';
  return code;
};


//bloque dentro del cual va el código que se va a ejecutar al hacer click sobre el boton iniciar juego
Blockly.JavaScript['frenar'] = function(block) {
  var statements_start = Blockly.JavaScript.statementToCode(block, 'frenar');
  // TODO: Assemble JavaScript into code variable.

  var code = 'frenar();\n';
  return code;
};