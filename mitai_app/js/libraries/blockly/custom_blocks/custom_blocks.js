/*BLOQUE FOR*/
Blockly.Blocks['bucleFor'] = {
  init: function() {
    Blockly.HSV_SATURATION = 0.8114;
    Blockly.HSV_VALUE = 0.992;
    this.setHelpUrl('http');
    this.setColour(14);
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldTextInput('10',
            Blockly.FieldTextInput.nonnegativeIntegerValidator), 'repeticiones')
        .appendField(new Blockly.FieldImage(BUCLE_FOR, 30, 30, "*"));
    this.appendStatementInput('hacer')
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('Repetir tantas veces');
  }
};


/*BLOQUE WHILE*/
Blockly.Blocks['bucleWhile'] = {
      /**
       * Block for 'do while/until' loop.
       * @this Blockly.Block
       */
      init: function() {
       Blockly.HSV_SATURATION = 0.8114;
    Blockly.HSV_VALUE = 0.992;
        //this.setHelpUrl(Blockly.Msg.CONTROLS_WHILEUNTIL_HELPURL);
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldImage(WHILE, 30, 30, "*"))
            this.appendValueInput('CONDICION')
            .setCheck('Boolean').setAlign(Blockly.ALIGN_RIGHT);
        this.appendStatementInput('HACER')
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(14);
        var thisBlock = this;
    
      }
    };


/*BLOQUE CONDICIONAL*/    
Blockly.Blocks['condicionalif'] = {
  /**
   * Block for if/elseif/else condition.
   * @this Blockly.Block
   * Modificado para utilizar imagenes en lugar de texto
   */
  init: function() {
    Blockly.HSV_SATURATION = 0.8114;
    Blockly.HSV_VALUE = 0.992;
    this.setHelpUrl(Blockly.Msg.CONTROLS_IF_HELPURL);
    this.setColour(14);
    this.appendValueInput('IF0')
        .setCheck('Boolean')
        .appendField(new Blockly.FieldImage(CONDICIONAL_IF, 30, 30, "*"));
    this.appendStatementInput('DO0')
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    //this.setMutator(new Blockly.Mutator(['controls_if_elseif',
     //                                    'controls_if_else']));
    // Assign 'this' to a variable for use in the tooltip closure below.
    var thisBlock = this;
    this.setTooltip(function() {
      if (!thisBlock.elseifCount_ && !thisBlock.elseCount_) {
        return Blockly.Msg.CONTROLS_IF_TOOLTIP_1;
      } else if (!thisBlock.elseifCount_ && thisBlock.elseCount_) {
        return Blockly.Msg.CONTROLS_IF_TOOLTIP_2;
      } else if (thisBlock.elseifCount_ && !thisBlock.elseCount_) {
        return Blockly.Msg.CONTROLS_IF_TOOLTIP_3;
      } else if (thisBlock.elseifCount_ && thisBlock.elseCount_) {
        return Blockly.Msg.CONTROLS_IF_TOOLTIP_4;
      }
      return '';
    });
    this.elseifCount_ = 0;
    this.elseCount_ = 0;
  },
  /**
   * Create XML to represent the number of else-if and else inputs.
   * @return {Element} XML storage element.
   * @this Blockly.Block
   */
  mutationToDom: function() {
    if (!this.elseifCount_ && !this.elseCount_) {
      return null;
    }
    var container = document.createElement('mutation');
    if (this.elseifCount_) {
      container.setAttribute('elseif', this.elseifCount_);
    }
    if (this.elseCount_) {
      container.setAttribute('else', 1);
    }
    return container;
  },
  /**
   * Parse XML to restore the else-if and else inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this Blockly.Block
   */
  domToMutation: function(xmlElement) {
    this.elseifCount_ = parseInt(xmlElement.getAttribute('elseif'), 10);
    this.elseCount_ = parseInt(xmlElement.getAttribute('else'), 10);
    for (var i = 1; i <= this.elseifCount_; i++) {
      this.appendValueInput('IF' + i)
          .setCheck('Boolean')
          .appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSEIF);
      this.appendStatementInput('DO' + i)
          .appendField(Blockly.Msg.CONTROLS_IF_MSG_THEN);
    }
    if (this.elseCount_) {
      this.appendStatementInput('ELSE')
          .appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSE);
    }
  },
  /**
   * Populate the mutator's dialog with this block's components.
   * @param {!Blockly.Workspace} workspace Mutator's workspace.
   * @return {!Blockly.Block} Root block in mutator.
   * @this Blockly.Block
   */
  decompose: function(workspace) {
    var containerBlock = Blockly.Block.obtain(workspace, 'controls_if_if');
    containerBlock.initSvg();
    var connection = containerBlock.getInput('STACK').connection;
    for (var i = 1; i <= this.elseifCount_; i++) {
      var elseifBlock = Blockly.Block.obtain(workspace, 'controls_if_elseif');
      elseifBlock.initSvg();
      connection.connect(elseifBlock.previousConnection);
      connection = elseifBlock.nextConnection;
    }
    if (this.elseCount_) {
      var elseBlock = Blockly.Block.obtain(workspace, 'controls_if_else');
      elseBlock.initSvg();
      connection.connect(elseBlock.previousConnection);
    }
    return containerBlock;
  },
  /**
   * Reconfigure this block based on the mutator dialog's components.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  compose: function(containerBlock) {
    // Disconnect the else input blocks and remove the inputs.
    if (this.elseCount_) {
      this.removeInput('ELSE');
    }
    this.elseCount_ = 0;
    // Disconnect all the elseif input blocks and remove the inputs.
    for (var i = this.elseifCount_; i > 0; i--) {
      this.removeInput('IF' + i);
      this.removeInput('DO' + i);
    }
    this.elseifCount_ = 0;
    // Rebuild the block's optional inputs.
    var clauseBlock = containerBlock.getInputTargetBlock('STACK');
    while (clauseBlock) {
      switch (clauseBlock.type) {
        case 'controls_if_elseif':
          this.elseifCount_++;
          var ifInput = this.appendValueInput('IF' + this.elseifCount_)
              .setCheck('Boolean')
              .appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSEIF);
          var doInput = this.appendStatementInput('DO' + this.elseifCount_);
          doInput.appendField(Blockly.Msg.CONTROLS_IF_MSG_THEN);
          // Reconnect any child blocks.
          if (clauseBlock.valueConnection_) {
            ifInput.connection.connect(clauseBlock.valueConnection_);
          }
          if (clauseBlock.statementConnection_) {
            doInput.connection.connect(clauseBlock.statementConnection_);
          }
          break;
        case 'controls_if_else':
          this.elseCount_++;
          var elseInput = this.appendStatementInput('ELSE');
          elseInput.appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSE);
          // Reconnect any child blocks.
          if (clauseBlock.statementConnection_) {
            elseInput.connection.connect(clauseBlock.statementConnection_);
          }
          break;
        default:
          throw 'Unknown block type.';
      }
      clauseBlock = clauseBlock.nextConnection &&
          clauseBlock.nextConnection.targetBlock();
    }
  },
  /**
   * Store pointers to any connected child blocks.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  saveConnections: function(containerBlock) {
    var clauseBlock = containerBlock.getInputTargetBlock('STACK');
    var i = 1;
    while (clauseBlock) {
      switch (clauseBlock.type) {
        case 'controls_if_elseif':
          var inputIf = this.getInput('IF' + i);
          var inputDo = this.getInput('DO' + i);
          clauseBlock.valueConnection_ =
              inputIf && inputIf.connection.targetConnection;
          clauseBlock.statementConnection_ =
              inputDo && inputDo.connection.targetConnection;
          i++;
          break;
        case 'controls_if_else':
          var inputDo = this.getInput('ELSE');
          clauseBlock.statementConnection_ =
              inputDo && inputDo.connection.targetConnection;
          break;
        default:
          throw 'Unknown block type.';
      }
      clauseBlock = clauseBlock.nextConnection &&
          clauseBlock.nextConnection.targetBlock();
    }
  }
};

/*BLOQUE AVANZAR*/
Blockly.Blocks['avanzar'] = {
  init: function() {
    Blockly.HSV_SATURATION = 1;
    Blockly.HSV_VALUE = 0.804;
    this.setHelpUrl('http://www.example.com/');
    this.setColour(108);
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage(AVANZAR, 50, 50, "*"))
        .appendField("\t")
        .appendField("!");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};  

/*BLOQUE GIRAR IZQUIERDA*/
Blockly.Blocks['girarIzquierda'] = {
  init: function() {
    Blockly.HSV_SATURATION = 1;
    Blockly.HSV_VALUE = 0.804;
    this.setColour(108);
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage(GIRAR_IZQUIERDA, 50, 50, "*"))
        .appendField("\t")
        .appendField("!");;
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('Girar Izquierda');
  }
};


/*BLOQUE GIRAR DERECHA*/
Blockly.Blocks['girarDerecha'] = {
  init: function() {
    Blockly.HSV_SATURATION = 1;
    Blockly.HSV_VALUE = 0.804;
    this.setColour(108);
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage(GIRAR_DERECHA, 50, 50, "*"))
        .appendField("\t")
        .appendField("!");;
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('Girar Derecha');
  }
};


/*BLOQUE PARE*/
Blockly.Blocks['logica_pare'] = {
  init: function() {
     Blockly.HSV_SATURATION = 0.709;
    Blockly.HSV_VALUE = 0.553;
    this.setColour(250);
    this.setOutput(true, 'Boolean');
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(PARE, 60, 60, "*"));
    this.setTooltip('Pare');
  }
};


/*BLOQUE SEMAFORO ROJO*/
Blockly.Blocks['logica_semaforoRojo'] = {
  init: function() {
     Blockly.HSV_SATURATION = 0.709;
    Blockly.HSV_VALUE = 0.553;
    this.setColour(250);
    this.setOutput(true, 'Boolean');
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(SEMAFORO_ROJO, 60, 60, "*"));
    this.setTooltip('Semaforo Rojo');
  }
};

/*BLOQUE SEMAFORO NARANJA*/
Blockly.Blocks['logica_semaforoNaranja'] = {
  init: function() {
     Blockly.HSV_SATURATION = 0.709;
    Blockly.HSV_VALUE = 0.553;
    this.setColour(250);
    this.setOutput(true, 'Boolean');
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(SEMAFORO_NARANJA, 60, 60, "*"));
    this.setTooltip('Semaforo Amarillo');
  }
};


/*BLOQUE SEMAFORO VERDE*/
Blockly.Blocks['logica_semaforoVerde'] = {
  init: function() {
     Blockly.HSV_SATURATION = 0.709;
    Blockly.HSV_VALUE = 0.553;
    this.setColour(250);
    this.setOutput(true, 'Boolean');
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(SEMAFORO_VERDE, 60, 60, "*"));
    this.setTooltip('Semaforo Verde');
  }
};

/*Bloque Start*/

Blockly.Blocks['start'] = {
  init: function() {
    Blockly.HSV_SATURATION = 1;
    Blockly.HSV_VALUE = 0.941;
    this.appendStatementInput("start")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage(START, 35, 35, "*"));
    this.setColour(192);
    this.setTooltip('Cuando empieza el juego');
    this.setHelpUrl('http://www.example.com/');
  }
};


/*FRENAR*/
Blockly.Blocks['frenar'] = {
  init: function() {
    Blockly.HSV_SATURATION = 1;
    Blockly.HSV_VALUE = 0.804;
    this.setHelpUrl('http://www.example.com/');
    this.setColour(108);
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage(FRENAR, 50, 50, "*"))
        .appendField("\t")
        .appendField("!");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};  